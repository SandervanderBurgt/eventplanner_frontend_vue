import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loggedIn: JSON.parse(<string>localStorage.getItem('loggedIn')),
    id: JSON.parse(<string>localStorage.getItem('id')),
    apiURL: 'http://localhost:8000',
    loading: false
  },
  mutations: {
    logout (state) {

      localStorage.removeItem('loggedIn')
      localStorage.removeItem('id')
      state.loggedIn = false
    },
    login (state, givenID) {
      localStorage.setItem('loggedIn', JSON.stringify(true))
      localStorage.setItem('id', JSON.stringify(givenID))
      state.id = JSON.parse(<string>localStorage.getItem('id'))
      state.loggedIn = true

    },
    changeLoading(state, value) {
      state.loading = value
    }
  },
  actions: {},
  getters: {
    apiURL: state => {
      return state.apiURL
    },
    id: state => {
      return state.id
    }
  }
})

