import Vue from 'vue'
import Router from 'vue-router'
import { RouterOptions, RouteConfig } from 'vue-router';
import Login from '@/views/Login.vue'
import Dashboard from '@/views/Dashboard.vue'
import store from '@/store'

Vue.use(Router)

const navRoutes = <Array<RouteConfig>>[
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    props: { eventMode: 2, pageTitle: 'Mijn afspraken' }, // 1: All events -- 2: Events without unsigned -- 3: only unsigned events
    beforeEnter: (to, from, next) => {
      store.commit('changeLoading', true)
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/invites',
    name: 'invites',
    component: Dashboard,
    props: { eventMode: 3, pageTitle: 'Mijn uitnodigingen' }, // 1: All events -- 2: Events without unsigned -- 3: only unsigned events
    beforeEnter: (to, from, next) => {
      store.commit('changeLoading', true)
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    props: true,
    beforeEnter: (to, from, next) => {
      if (!store.state.loggedIn) next()
      else next('/')
    }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "about" */ './views/Register.vue'),
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    beforeEnter: (to, from, next) => {
      if (!store.state.loggedIn) next()
      else next('/')
    }
  },
  {
    path: '/event/:eventid',
    name: 'event',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/Event.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/profile',
    name: 'profile',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/Profile.vue'),
    props: true,
    beforeEnter: (to, from, next) => {
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/profile/edit-password',
    name: 'profile-edit-password',
    props: { reset: false, title: 'Account wijzigen' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/EditPassword.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/reset-password',
    name: 'reset-password',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/ResetPassword.vue')
  },
  {
    path: '/reset-password/:tokenId',
    name: 'reset-password-with-token',
    props: { reset: true, title: 'Wachtwoord herstellen' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/EditPassword.vue'),
    beforeEnter: (to, from, next) => {
      if (to.params.tokenId.length === 36) next()
      else next('/reset-password')
    }
  },
  {
    path: '/verify-account/:tokenId',
    name: 'verify-account-with-token',
    props: { title: 'Account verifiëren' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/VerifyAccount.vue'),
    beforeEnter: (to, from, next) => {
      if (to.params.tokenId.length === 36) next()
      else next('/login')
    }
  },
  {
    path: '/new',
    name: 'new',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/NewEvent.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '/editevent/:eventid',
    name: 'editevent',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/NewEvent.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.loggedIn) next()
      else next('/login')
    }
  },
  {
    path: '*',
    name: 'None',
    component: Login
  }
]

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: navRoutes
})

router.afterEach(() => {
  setTimeout(function () {
    store.commit('changeLoading', false)
  }, 1500)
})

export default router
