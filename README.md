# GathrUp VueJS frontend application

## Project setup
```
npm install
```

Rename .env.local-EXAMPLE to .env.local and replace the Javascript Google Maps API key


### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
